package com.dashinvestments.serviceapp;

import com.orm.SugarRecord;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Zed on 11/23/2016.
 */

public class Product extends SugarRecord implements Serializable {
    private static String TAG = Product.class.getSimpleName();

    public String name;
    public int price;
    public int quantity;
    public long supermarketId;
    public int salePrice;

    public Product(){}

//    public Product(OrderItem p){
//        this.quantity = p.quantity;
//        this.price = p.price;
//        this.salePrice = 0;
//        this.name = p.name;
//    }

    public Product(JSONObject json){
        try{
            this.setId(json.getLong("id"));
            this.name = json.getString("name");
            this.price = json.optInt("price", 0);
            this.salePrice = json.optInt("sale_price", 0);
        }catch (Exception ex){
            //Utils.log(TAG, "Error initializing price -> " + ex.getMessage());
        }
    }

    public int getPrice(){
        return this.salePrice > 0 ? this.salePrice : this.price;
    }
}
