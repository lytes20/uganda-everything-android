package com.dashinvestments.serviceapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by gideon_bamuleseyo on 10/31/17.
 */

public class AllCategoriesAdapter extends BaseAdapter {

    private final Context mContext;
    private LayoutInflater inflater;
    public String [] allCategories;

    public AllCategoriesAdapter(Context mContext, String[] allCategories) {
        this.mContext = mContext;
        this.allCategories = allCategories;
    }

    @Override
    public int getCount() {
        return allCategories.length;
    }

    @Override
    public Object getItem(int i) {
        return allCategories[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View listView = view;
        if(view == null){

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listView = inflater.inflate(R.layout.all_categories_item, null);
        }
        TextView subCategoryName = (TextView) listView.findViewById(R.id.category_item);
        subCategoryName.setText(allCategories[i]);

        return listView;
    }

}
