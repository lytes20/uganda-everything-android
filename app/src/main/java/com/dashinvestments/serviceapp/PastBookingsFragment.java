package com.dashinvestments.serviceapp;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;

/**
 * Created by gideon_bamuleseyo on 10/24/17.
 */

public class PastBookingsFragment extends Fragment{

    AQuery aq;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_past_bookings, container, false);
//        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//        textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        aq = new AQuery(getContext());

        String [] services = {"", "", ""};
        ListView listView = (ListView) rootView.findViewById(R.id.pastbookings_list);
        BookingsAdapter bookingsAdapter = new BookingsAdapter(getActivity(), services);
        listView.setAdapter(bookingsAdapter);

        boolean hasBookings = false;

        if(!hasBookings){
            listView.setVisibility(View.GONE);
            //aq.id(R.id.pastbookings_list).gone();
        }else {

        }

        return rootView;
    }


}
