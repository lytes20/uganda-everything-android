package com.dashinvestments.serviceapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class SignUpActivity extends AppCompatActivity {

    AQuery aq;
    UEDatabaseHelper ueDatabaseHelper;

    private FirebaseAuth firebaseAuth;
    ProgressDialog dialog;

    FirebaseDatabase database;
    DatabaseReference myRef;

    LogInHandler logInHandler;

    User mUser;

    boolean isSignUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        logInHandler = new LogInHandler(this);
        isSignUp = true;

        firebaseAuth = FirebaseAuth.getInstance();
        aq =  new AQuery(this);


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        boolean logedInState = sharedPreferences.getBoolean("is_loggeg_in", Boolean.parseBoolean(Constants.DEFAULT_STRING));
        if(logedInState){
            aq.id(R.id.terms_container).gone();
            aq.id(R.id.txt_password).gone();
            aq.id(R.id.terms).gone();
            aq.id(R.id.btn_submit).getButton().setText("Update");

            //populating fields if user is logged in
            aq.id(R.id.txt_first_name).getEditText().setText(sharedPreferences.getString("first_name",Constants.DEFAULT_STRING));
            aq.id(R.id.txt_last_name).getEditText().setText(sharedPreferences.getString("last_name",Constants.DEFAULT_STRING));
            aq.id(R.id.txt_username).getEditText().setText(sharedPreferences.getString("user_name",Constants.DEFAULT_STRING));
            aq.id(R.id.txt_email).getEditText().setText(sharedPreferences.getString("email",Constants.DEFAULT_STRING));
            aq.id(R.id.txt_phone_number).getEditText().setText(sharedPreferences.getString("phone",Constants.DEFAULT_STRING));
            //aq.id(R.id.txt_password).getEditText().setText(userSharedPreferences.getString("password",Constants.DEFAULT_STRING));

            aq.id(R.id.btn_submit).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    processNewEnteredDetails();

                }
            });
        }else{
            aq.id(R.id.btn_submit).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    processRegistration();

                }
            });
        }


    }

    private void processNewEnteredDetails() {
        String firstName = aq.id(R.id.txt_first_name).getText().toString().trim();
        String lastName = aq.id(R.id.txt_last_name).getText().toString().trim();
        String username = aq.id(R.id.txt_username).getText().toString().trim();
        final String email = aq.id(R.id.txt_email).getText().toString().trim();
        String phone = aq.id(R.id.txt_phone_number).getText().toString().trim();

        SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("first_name",firstName);
        editor.putString("last_name",lastName);
        editor.putString("user_name",username);
        editor.putString("email",email);
        editor.putString("phone",phone);


        editor.commit();

        showErrorToast("Your profile has been updated");

    }


    private void processRegistration(){
        // HashMap<String, Object> params = new HashMap<>();

        String firstName = aq.id(R.id.txt_first_name).getText().toString().trim();
        String lastName = aq.id(R.id.txt_last_name).getText().toString().trim();
        String username = aq.id(R.id.txt_username).getText().toString().trim();
        final String email = aq.id(R.id.txt_email).getText().toString().trim();
        String phone = aq.id(R.id.txt_phone_number).getText().toString().trim();
        final String password = aq.id(R.id.txt_password).getText().toString().trim();
        boolean agreed = aq.id(R.id.terms).isChecked();

        String sex;
        int sexSelection = aq.id(R.id.spn_sex).getSelectedItemPosition();

        if(firstName.isEmpty()){
            showErrorToast("Please enter your first name");
            return;
        }
        if(lastName.isEmpty()){
            showErrorToast("Please enter your last name");
            return;
        }
        if(phone.length() < 3){
            showErrorToast("Please enter a valid phone number");
            return;
        }
        if(!email.isEmpty() && !Utils.isValidEmail(email)){
            showErrorToast("Please enter a valid email address");
            return;
        }

        if(password.isEmpty()){
                showErrorToast("Please enter your password");
                return;
        }
        if(password.length() < 6){
            showErrorToast("Weak Password");
        }
        if(username.isEmpty()){
                showErrorToast("Please enter your username");
                return;
        }


        if(sexSelection == 0){
            sex = "N/A";
        }else{
            sex = aq.id(R.id.spn_sex).getSelectedItem().toString();
        }
//        if(dateString == null){
//            dateString = "";
//        }

        SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("first_name",firstName);
        editor.putString("last_name",lastName);
        editor.putString("user_name",username);
        editor.putString("email",email);
        editor.putString("phone",phone);
        editor.putString("password",password);
        editor.putString("location", "");

        editor.commit();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Registering, Please wait....");
        dialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            //open mainActivity
                            dialog.dismiss();

                            logInHandler.logInWithFireBase(email, password, isSignUp);

                            //startActivity(new Intent(SignUpActivity.this, LoginActivity.class));

                        }else{
                            dialog.dismiss();
                            Log.w("MY TAG", "createUserWithEmail:failure", task.getException());
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                showErrorToast("Password should be atleast 6 characters");
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                showErrorToast("Invalid email");
                            } catch(FirebaseAuthUserCollisionException e) {
                                showErrorToast("Email not available");
                            } catch(Exception e) {
                                Log.e("MY:", e.getMessage());
                            }

                        }

                    }
                });


        // startActivity(new Intent(SignUpActivity.this, LoginActivity.class));


    }


    public void upDateUserInfo(){

    }



    private void showErrorToast(String error){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
