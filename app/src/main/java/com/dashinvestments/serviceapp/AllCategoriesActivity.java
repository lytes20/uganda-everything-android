package com.dashinvestments.serviceapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by gideon_bamuleseyo on 10/31/17.
 */

public class AllCategoriesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);

        ListView listView = (ListView) findViewById(R.id.all_categories_list);

        final String [] allCategories = {"Saloons","Hotels", "Restaurants", "Mechanics","Bars and Clubs", "Hospitals"};

        AllCategoriesAdapter allCategoriesAdapter = new AllCategoriesAdapter(AllCategoriesActivity.this,  allCategories);
        listView.setAdapter(allCategoriesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = null;
                intent = new Intent(AllCategoriesActivity.this, SubCategories.class);
                intent.putExtra("Category", allCategories[i]);
                startActivity(intent);
            }
        });
    }
}
