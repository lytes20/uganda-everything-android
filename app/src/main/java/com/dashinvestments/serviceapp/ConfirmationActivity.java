package com.dashinvestments.serviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/21/17.
 */

public class ConfirmationActivity extends AppCompatActivity {

    AQuery aq;
    public static final String DEFAULT = "N/A";


    DatabaseReference myRef;
    FirebaseDatabase database;
    Booking mBooking;

    TextView serviceNameDisplay, serviceLocationDisplay, serviceDisplay, costDisplay, dateDisplay, timeDisplay, userNameDisplay, phoneDisplay;

    SharedPreferences userSharedPreferences, serviceSharedPreferences;

    String fullName, categoryName, service, dateSelected, phoneNumber;
    String serviceName;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        aq = new AQuery(this);


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Bookings");

        serviceNameDisplay = (TextView) findViewById(R.id.service_name_display);
        serviceLocationDisplay = (TextView) findViewById(R.id.service_location_display);
        serviceDisplay = (TextView) findViewById(R.id.service_display);
        costDisplay = (TextView) findViewById(R.id.cost_display );
        dateDisplay = (TextView) findViewById(R.id.date_display);
        timeDisplay = (TextView) findViewById(R.id.time_display);
        userNameDisplay = (TextView) findViewById(R.id.name_display);
        phoneDisplay = (TextView) findViewById(R.id.phone_display);




        userSharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);

        serviceSharedPreferences =  getSharedPreferences("service_selected", Context.MODE_PRIVATE);


        //SharedPreferences servicesPrefs = getSharedPreferences("User", Context.MODE_PRIVATE);

        fullName = userSharedPreferences.getString("first_name", DEFAULT) +" "+ userSharedPreferences.getString("last_name", DEFAULT);
        phoneNumber = userSharedPreferences.getString("phone", DEFAULT);
        String username = userSharedPreferences.getString("username", DEFAULT);

        dateSelected = serviceSharedPreferences.getString("appointment_date", Constants.DEFAULT_STRING);
        String timeSelected = serviceSharedPreferences.getString("appointment_time", Constants.DEFAULT_STRING);
        String cost = "";
        service = serviceSharedPreferences.getString("service_desc", Constants.DEFAULT_STRING);
        serviceName = serviceSharedPreferences.getString("service_name", Constants.DEFAULT_STRING);
        String serviceLocation = serviceSharedPreferences.getString("service_location", Constants.DEFAULT_STRING);
        categoryName = "";


        userNameDisplay.setText(fullName);
        phoneDisplay.setText(phoneNumber);
        dateDisplay.setText(dateSelected);
        timeDisplay.setText(timeSelected);
        costDisplay.setText(cost);
        serviceDisplay.setText(service);
        serviceLocationDisplay.setText(serviceLocation);
        serviceNameDisplay.setText(serviceName);






        aq.id(R.id.btn_submit_details).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                mBooking = new  Booking(fullName, categoryName, service, dateSelected, phoneNumber, currentDateTimeString, serviceName);

                DatabaseReference newBooking = myRef.push();
                newBooking.setValue(mBooking);
                //myRef.push().child("Booking").setValue(mBooking);
                //myRef.setValue("Hello, World!");
                startActivity(new Intent(ConfirmationActivity.this, SuccessActivity.class));
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
