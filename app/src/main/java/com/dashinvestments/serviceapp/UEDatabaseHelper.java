package com.dashinvestments.serviceapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by gideon_bamuleseyo on 10/29/17.
 */

public class UEDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "uedatabase";
    public static final String USER_TABLE_NAME = "USERTABLE";
    public static final int DATABASE_VERSION = 1;
    public static final String UID = "_id";

    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String USER_COLUMN_FIRST_NAME = "first_name";
    public static final String USER_COLUMN_LAST_NAME= "last_name";
    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_PHONE = "phone";
    public static final String USER_COLUMN_EMAIL = "email";
    public static final String USER_COLUMN_DOB = "dob";
    public static final String USER_COLUMN_SEX = "sex";
    public static final String USER_COLUMN_PASSWORD = "phone";

    public static final String CREATE_TABLE = "CREATE TABLE "+USER_TABLE_NAME+" ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+USER_COLUMN_FIRST_NAME+" VARCHAR(255));";
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS" +USER_TABLE_NAME;

    private Context context;

    public UEDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        showToast(context, "Constructor was called");


    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        showToast(context, "Database was created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
        showToast(context, "Database was created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_TABLE);
        onCreate(sqLiteDatabase);

    }

    private void showToast(Context context, String message){
        Toast.makeText(context , message, Toast.LENGTH_SHORT).show();
    }
}
