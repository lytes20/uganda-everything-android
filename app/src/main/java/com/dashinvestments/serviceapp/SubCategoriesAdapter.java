package com.dashinvestments.serviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class SubCategoriesAdapter extends BaseAdapter{
    private final Context mContext;
    private LayoutInflater inflater;
    public ArrayList<String> subCategories;
    //public int [] imageLogos;
    public ArrayList<String> phones;
    public ArrayList<String> locations;
    public ArrayList<String> services;
    public ArrayList<String> imageUrls;

    AQuery aq;

    public SubCategoriesAdapter(Context mContext, ArrayList<String> subCategories) {
        this.mContext = mContext;
        this.subCategories = subCategories;
    }

    public SubCategoriesAdapter(Context mContext, ArrayList<String> subCategories, ArrayList<String> phones, ArrayList<String> locations, ArrayList<String> services, ArrayList<String> imageUrls) {
        this.mContext = mContext;
        this.subCategories = subCategories;
        this.imageUrls =  imageUrls;
        this.phones = phones;
        this.locations = locations;
        this.services = services;
    }

    @Override
    public int getCount() {
        return subCategories.size();
    }

    @Override
    public Object getItem(int i) {
        return subCategories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View listView = view;

        AQuery aq = new AQuery(view);
        if(view == null){

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listView = inflater.inflate(R.layout.subcategories_item, null);
        }
        TextView subCategoryName = (TextView) listView.findViewById(R.id.nameTextView);
        subCategoryName.setText(subCategories.get(i));


        aq.id(R.id.companyLogo).image(imageUrls.get(i));




        TextView phone = listView.findViewById(R.id.phone_number_tv);
        TextView location = listView.findViewById(R.id.location_tv);
        TextView servicesTv = listView.findViewById(R.id.services_tv);

        phone.setText(phones.get(i));
        location.setText(locations.get(i));
        servicesTv.setText(services.get(i));

        Button appointmentButton = (Button) listView.findViewById(R.id.appointmentButton);
        appointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                SharedPreferences sharedPreferences = mContext.getSharedPreferences("service_selected", Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("service_name",subCategories.get(i));
                editor.putString("service_location",locations.get(i));
                editor.putString("requested_service",services.get(i));
                editor.putString("selected_service_image", imageUrls.get(i));
                editor.commit();
                mContext.startActivity(new Intent(mContext, MakeBooking.class));
            }
        });

        return listView;
    }
}
