package com.dashinvestments.serviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.androidquery.AQuery;
import com.google.firebase.auth.FirebaseAuth;
import com.pixplicity.easyprefs.library.Prefs;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String DEFAULT = "N/A";
    AQuery aq;
    NavigationView mNavigationView;
    Menu mDrawerMenu;
    MenuItem mNavLogin;

    int[] flipImages = { R.drawable.crasted_crane, R.drawable.equator,
            R.drawable.kampala_night_one, R.drawable.kampala_night_two };
    private ViewFlipper myViewFlipper;
    private float initialXPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        aq = new AQuery(this);




        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mDrawerMenu = mNavigationView.getMenu();
        mNavLogin = mDrawerMenu.findItem(R.id.nav_login);

        View header = mNavigationView.getHeaderView(0);
        AQuery a = new AQuery(header);

        myViewFlipper = (ViewFlipper) findViewById(R.id.imageView2);

        for (int i = 0; i < flipImages.length; i++) {
            ImageView imageView = new ImageView(MainActivity.this);
            imageView.setImageResource(flipImages[i]);
            myViewFlipper.addView(imageView);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        myViewFlipper.setAutoStart(true);
        myViewFlipper.setFlipInterval(3000);
        myViewFlipper.startFlipping();



        SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        boolean loggegInState = sharedPreferences.getBoolean("is_loggeg_in", Boolean.parseBoolean(DEFAULT));
        if(!loggegInState){
            a.id(R.id.drawer_user_name).text("");
            a.id(R.id.drawer_email).text("");
            mNavLogin.setTitle("Log In");
        }else{

            String stringUrl = Prefs.getString("user_profile_pic", Constants.DEFAULT_STRING);
            Log.d("Profile Pic", stringUrl);
            if(stringUrl != Constants.DEFAULT_STRING) {
                a.id(R.id.profile_image).image(stringUrl);
            }

            a.id(R.id.drawer_user_name).text(sharedPreferences.getString("first_name", DEFAULT) +" "+ sharedPreferences.getString("last_name", DEFAULT));
            a.id(R.id.drawer_email).text(sharedPreferences.getString("email", DEFAULT));
            mNavLogin.setTitle("Log Out");
        }



        final String [] categories = {"Salons","Hotels", "Restaurants", "Recreation & Getways","Clubs & Bars", "See More ...."};
        final int [] categoriesIcons = {R.drawable.icon_saloon,R.drawable.room_key, R.drawable.dinner, R.drawable.trampoline,R.drawable.glass, R.drawable.see_more};

        GridView categoriesGrid = (GridView) findViewById(R.id.categotriesGridView);
        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(MainActivity.this, categories, categoriesIcons);
        categoriesGrid.setAdapter(categoriesAdapter);

        categoriesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = null;
                switch(i){
                    case 5:
                        intent = new Intent(MainActivity.this, AllCategoriesActivity.class);
                        break;
                    default:
                        intent = new Intent(MainActivity.this, SubCategories.class);

                        break;
                }
                intent.putExtra("Category", categories[i]);
                startActivity(intent);
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        Intent i = null;
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            i = new Intent(this, MainActivity.class);
        } else if (id == R.id.nav_contact_us) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:0775921692"));
            startActivity(intent);

        }  else if (id == R.id.nav_web) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.WEBSITE_URL));
            startActivity(browserIntent);

        }
//        else if(id == R.id.nav_bookings){
//            i = new Intent(this, BookingsActivity.class);
//        }
        else if(id == R.id.nav_login){
            SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
            boolean loggegInState = sharedPreferences.getBoolean("is_loggeg_in", Boolean.parseBoolean(DEFAULT));
            if(!loggegInState){

                i = new Intent(this, LoginActivity.class);

            }else{
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();

                FirebaseAuth.getInstance().signOut();

                i = new Intent(this, MainActivity.class);
                finish();
            }


        }else if (id == R.id.nav_profile){
            i =  new Intent(this, UserProfileActivity.class);
        }

        if(i != null){
            startActivity(i);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialXPoint = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalx = event.getX();
                if (initialXPoint > finalx) {
                    if (myViewFlipper.getDisplayedChild() == flipImages.length)
                        break;
                    myViewFlipper.showNext();
                } else {
                    if (myViewFlipper.getDisplayedChild() == 0)
                        break;
                    myViewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
