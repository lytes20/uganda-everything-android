package com.dashinvestments.serviceapp;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by gideon_bamuleseyo on 11/2/17.
 */

public class LogInHandler {
    //FirebaseAuth mAuth;
    private Activity mContext;


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();


    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public LogInHandler(){

    }



    public LogInHandler(Activity mContext) {
        this.mContext = mContext;
    }

    public void logInWithFireBase(final String enteredEmail, String enteredPassword, final boolean isSignUp){

        mAuth.signInWithEmailAndPassword(enteredEmail, enteredPassword)
                .addOnCompleteListener(mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            if(isSignUp == true){
                                createUserInDatabase(enteredEmail);
                            }

                            Intent i = new Intent();
                            i.setClass(mContext, MainActivity.class);
                            mContext.startActivity(i);



                        }else{
                            showErrorToast("Incorrect email or password");
                        }

                    }
                });


    }

    public void createUserInDatabase(String email){
        myRef.child("Joey").setValue(email);

    }

    private void showErrorToast(String error){
        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
    }

}
