package com.dashinvestments.serviceapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.pixplicity.easyprefs.library.Prefs;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class LoginActivity extends AppCompatActivity {
    AQuery aq;
    String password, username, email;
    User mUser;
    LogInHandler logInHandler;

    public static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    public final String TAG = "UGANDA EVERYTHING";

    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;
    ProgressDialog dialog;

    boolean isSignUp;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        isSignUp = false;

        mAuth = FirebaseAuth.getInstance();

        aq = new AQuery(this);
        mUser = new User();
        logInHandler = new LogInHandler(this);





        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if(mAuth.getCurrentUser() != null){
                    SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    mUser.setFirstName("");
                    mUser.setLastName("");

                    //String fullName = currentUser.getDisplayName();
                    if (currentUser.getDisplayName() != null){
                        String fullName = currentUser.getDisplayName();

                        if(fullName.contains(" ")){
                            String[] spilttedName = fullName.split("\\s+");
                            mUser.setFirstName(spilttedName[0]);
                            mUser.setLastName(spilttedName[1]);
                        }else{
                            mUser.setFirstName(fullName);
                            mUser.setLastName("");
                        }

                    }else {
                        mUser.setFirstName(currentUser.getEmail());
                        mUser.setLastName("");
                    }


                    editor.putString("first_name", mUser.getFirstName());
                    editor.putString("last_name",mUser.getLastName());
                    editor.putString("user_name","");
                    editor.putString("email", currentUser.getEmail());
                    //editor.putString("phone",currentUser.getPhoneNumber());

                    editor.putBoolean("is_loggeg_in", true);
                    editor.commit();
                    Log.d(TAG, String.valueOf(currentUser));

                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();

                }
            }
        };



        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        //show what is wrong

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        aq.id(R.id.txt_signup).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        aq.id(R.id.btn_submit).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processLogin();

            }
        });

        aq.id(R.id.google_login).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        aq.id(R.id.facebook_login).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showErrorToast("Facebook login unavailable");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
       // FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);

        mAuth.addAuthStateListener(mAuthListener);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("GOOGLE Login", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            Prefs.putString("user_profile_pic", String.valueOf(currentUser.getPhotoUrl()));
                            Log.d(TAG, String.valueOf(currentUser.getPhotoUrl()));
                            Log.d(TAG, currentUser.getEmail());
                            Log.d(TAG, currentUser.getDisplayName());
                            updateUI(currentUser);



                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("googleLoginFailure", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    public void updateUI(FirebaseUser user){
        if(user != null){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            return;
        }else{

        }
    }

    private void processLogin(){
        //username = aq.id(R.id.txt_username).getText().toString().trim();
        email = aq.id(R.id.txt_username).getText().toString().trim();
        password = aq.id(R.id.txt_password).getText().toString().trim();




        if(email.isEmpty()){
            showErrorToast("Please enter your username");
            return;
        }
        if(password.isEmpty()){
            showErrorToast("Please enter your password");
            return;
        }

        logInHandler.logInWithFireBase(email, password, isSignUp);


    }



    private void showErrorToast(String error){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
