package com.dashinvestments.serviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

/**
 * Created by gideon_bamuleseyo on 10/13/17.
 */

public class UserProfileActivity extends AppCompatActivity {

    AQuery aq;
    public static final String DEFAULT = "N/A";

    ImageView mEditProfile;
    TextView mEditProfileTV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        aq = new AQuery(this);

        mEditProfileTV = (TextView) findViewById(R.id.edit_profile);

        SharedPreferences sharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);

        aq.id(R.id.txt_name).text(sharedPreferences.getString("first_name", DEFAULT) + " " + sharedPreferences.getString("last_name", DEFAULT));
        aq.id(R.id.txt_username).text(sharedPreferences.getString("user_name", DEFAULT));
        aq.id(R.id.txt_phone).text(sharedPreferences.getString("phone", DEFAULT));
        aq.id(R.id.txt_email).text(sharedPreferences.getString("email", DEFAULT));
        aq.id(R.id.txt_location).text(sharedPreferences.getString("location", DEFAULT));
        aq.id(R.id.txt_username_two).text(sharedPreferences.getString("user_name", DEFAULT));

        mEditProfileTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserProfileActivity.this, SignUpActivity.class));
            }
        });
    }

}
