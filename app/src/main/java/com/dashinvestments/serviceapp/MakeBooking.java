package com.dashinvestments.serviceapp;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;

import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class MakeBooking extends AppCompatActivity {

    AQuery aq;
    User user;
    EditText firstname, lastname, phone, location, usernameET;
    String dateString = ""; String timeString = "";
    public static final String DEFAULT = "N/A";

    //List<Area> areas;
    //List<Supermarket> supermarkets;

    Spinner servicesSpinner, timeSlotsSpinner;
    String [] servicesArray, timeSlotsArray;
    SharedPreferences userSharedPreferences, serviceSharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userSharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        serviceSharedPreferences =  getSharedPreferences("service_selected", Context.MODE_PRIVATE);

        Log.d(Constants.LOG_TAG, serviceSharedPreferences.getString("service_name", Constants.DEFAULT_STRING) );
        boolean loggegInState = userSharedPreferences.getBoolean("is_loggeg_in", Boolean.parseBoolean(DEFAULT));
        if(!loggegInState){
           Intent i = new Intent(this, LoginActivity.class);

            startActivity(i);
            finish();
            return;
        }
        setContentView(R.layout.activity_make_booking);

        aq = new AQuery(this);
        user = new User();

        servicesArray = new String[]{"", ""};
        timeSlotsArray = new String []{"--select preferred time--", "9am", "10am", "11am", "12:00 midday", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm ", "8pm"};

        firstname = (EditText) findViewById(R.id.first_name_et);
        lastname = (EditText) findViewById(R.id.last_name_et);
        phone = (EditText) findViewById(R.id.phone_number_et);
        location =(EditText) findViewById(R.id.location_et);
        //usernameET = (EditText)findViewById(R.id.user);

        //SharedPreferences userSharedPreferences = getSharedPreferences("User", Context.MODE_PRIVATE);

        setDatePickers();

        String selectedImageUrl = serviceSharedPreferences.getString("selected_service_image", DEFAULT);
        String selectedServiceName = serviceSharedPreferences.getString("service_name", DEFAULT);

        aq.id(R.id.imageView5).image(selectedImageUrl);
        this.setTitle(selectedServiceName);

        user.setFirstName(userSharedPreferences.getString("first_name", DEFAULT));
        user.setLastName(userSharedPreferences.getString("last_name", DEFAULT));
        user.setPhoneNumber(userSharedPreferences.getString("phone", DEFAULT));



        if(!user.getFirstName().equals(DEFAULT)){
            firstname.setText(user.getFirstName());
        }
        if(!user.getLastName().equals(DEFAULT)){
            lastname.setText(user.getLastName());

        }
        if(!user.getPhoneNumber().equals(DEFAULT)){
            phone.setText(user.getPhoneNumber());

        }

//        servicesSpinner = aq.id(R.id.spn_services).getSpinner();
//        setDefaultsForSpinner(servicesSpinner, "Select Service", false);
//        getServices();



        timeSlotsSpinner = aq.id(R.id.spn_time).getSpinner();
        setDefaultsForSpinner(timeSlotsSpinner, "Select Time Slot", false);
        getTimeSlots();

        aq.id(R.id.btn_continue).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                processEnteredInformation();

            }
        });
    }


    public void processEnteredInformation(){

        SharedPreferences.Editor userEditor = userSharedPreferences.edit();
        SharedPreferences.Editor serviceEditor = serviceSharedPreferences.edit();

        String preferredFirstName = aq.id(R.id.first_name_et).getEditText().getText().toString();
        String preferredfLastName = aq.id(R.id.last_name_et).getEditText().getText().toString();
        String preferredPhoneNumber = aq.id(R.id.phone_number_et).getEditText().getText().toString();
        String preferredService = aq.id(R.id.enter_service_et).getEditText().getText().toString();
        String userLocation = aq.id(R.id.location_et).getEditText().getText().toString();


        if(preferredFirstName.isEmpty()){
            showErrorToast("Please enter your first name");
            return;
        }
        if(preferredfLastName.isEmpty()){
            showErrorToast("Please enter your last name");
            return;
        }
        if(preferredPhoneNumber.length() < 3){
            showErrorToast("Please enter a valid phone number");
            return;
        }

        if(preferredService.isEmpty()){
            showErrorToast("Enter some info on service");
            return;
        }

        if(dateString.isEmpty()){
            showErrorToast("Please select a date");
            return;
        }

        if(timeString.isEmpty()){
            showErrorToast("Please pick a time");
            return;
        }





        userEditor.putString("first_name", preferredFirstName);
        userEditor.putString("last_name", preferredfLastName);
        //editor.putString("user_name",username);
        userEditor.putString("phone", preferredPhoneNumber);
        userEditor.putString("location", userLocation);

        serviceEditor.putString("appointment_date", dateString);
        serviceEditor.putString("appointment_time", timeString);
        serviceEditor.putString("service_desc", preferredService);

        serviceEditor.commit();

        userEditor.commit();

        startActivity(new Intent(MakeBooking.this, ConfirmationActivity.class));

    }



    public void getTimeSlots(){
        ArrayAdapter<String> adapter = new DefaultSpinnerAdapter(this, android.R.layout.simple_list_item_1, timeSlotsArray);
        timeSlotsSpinner.setEnabled(true);
        timeSlotsSpinner.setAdapter(adapter);


        timeSlotsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0){
                    timeString = adapterView.getItemAtPosition(i).toString();
                }else{
                    timeString = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setDatePickers(){
        aq.id(R.id.appointment_date).getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    new DatePickerDialog(MakeBooking.this, new DatePickerDialog.OnDateSetListener(){
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            dateString = Utils.formatDate(year, monthOfYear, dayOfMonth, "d MMM, yyyy");
                            aq.id(R.id.appointment_date).text(dateString);
                        }
                    }, year, month, day).show();
                    return true;
                }
                return false;
            }
        });
    }

    public void processUserDetails(){




    }

    private void setDefaultsForSpinner(Spinner spinner, String text, boolean enabled){
        String[] listArray = new String[]{text};

        ArrayAdapter<String> adapter = new DefaultSpinnerAdapter(this, android.R.layout.simple_list_item_1, listArray);
        spinner.setAdapter(adapter);
        spinner.setEnabled(enabled);
    }

    private void showErrorToast(String error){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
