package com.dashinvestments.serviceapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class SubCategories extends AppCompatActivity {

    AQuery aq;
    ListView subCatListView;

    //Restaurants
    ArrayList<String> restaurantNames = new ArrayList<>();
    ArrayList<String> resaurantTelephone = new ArrayList<>();
    ArrayList<String> restaurantLocation = new ArrayList<>();
    ArrayList<String> restaurantServices = new ArrayList<>();
    ArrayList<String> restaurantImages = new ArrayList<>();


    //Hotel
    ArrayList<String> hotelNames = new ArrayList<>();
    ArrayList<String> hotelTelephone = new ArrayList<>();
    ArrayList<String> hotelLocation = new ArrayList<>();
    ArrayList<String> hotelServices = new ArrayList<>();
    ArrayList<String> hotelImages = new ArrayList<>();

    //Saloons
    ArrayList<String> saloonNames = new ArrayList<>();
    ArrayList<String> saloonTelephone = new ArrayList<>();
    ArrayList<String> saloonLocation = new ArrayList<>();
    ArrayList<String> saloonServices = new ArrayList<>();
    ArrayList<String> saloonImages = new ArrayList<>();

    //Bars
    ArrayList<String> barsNames = new ArrayList<>();
    ArrayList<String> barsTelephone = new ArrayList<>();
    ArrayList<String> barLocation = new ArrayList<>();
    ArrayList<String> barServices = new ArrayList<>();
    ArrayList<String> barImages = new ArrayList<>();

    //Mechanics
    ArrayList<String> mechanicsNames = new ArrayList<>();
    ArrayList<String> mechanicsTelephone = new ArrayList<>();
    ArrayList<String> mechanicsLocation = new ArrayList<>();
    ArrayList<String> mechanicsServices = new ArrayList<>();
    ArrayList<String> mechanicsImages = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subcategories);

        aq = new AQuery(this);
        subCatListView = (ListView) findViewById(R.id.subCategoriesListView);


       getSubCats();

        subCatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });




    }

    public void getSubCats(){
        aq.id(R.id.prog).visible();
        aq.id(R.id.subCategoriesListView).gone();


        final SubCategoriesAdapter subCategoriesAdapter = null;

        final ArrayList<String> listTitles = new ArrayList<>();



        String url = Constants.BASE_URL_PLAIN;
        aq.ajax(url,JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object != null){

                    String[] results = null;
                    Log.d("tag", String.valueOf(object));
                    JSONObject mJsonObject = null;

                    Gson gson = new GsonBuilder().create();


                        try {
                            String jsonResponse = object.getJSONArray("results").toString();
                            JSONArray resultsJsonArray = object.getJSONArray("results");
                            Log.d("tag", jsonResponse);
                            // results = gson.fromJson(jsonResponse, String[].class);

                            for(int i = 0; i < resultsJsonArray.length(); i++){
                                mJsonObject = resultsJsonArray.getJSONObject(i);

                                String serviceCat = mJsonObject.getString("service_category");
                                Log.d("jsonObjects", String.valueOf(mJsonObject));
                                Log.d("ServiceCats", serviceCat);
                                switch (serviceCat){
                                    case "Hotel":
                                        hotelNames.add(mJsonObject.getString("service_name"));
                                        hotelTelephone.add(mJsonObject.getString("service_phone"));
                                        hotelLocation.add(mJsonObject.getString("service_location"));
                                        hotelServices.add(mJsonObject.getString("service_description"));
                                        hotelImages.add(mJsonObject.getString("featured_service_image"));

                                        break;
                                    case "Restaurant":
                                        restaurantNames.add(mJsonObject.getString("service_name"));
                                        resaurantTelephone.add(mJsonObject.getString("service_phone"));
                                        restaurantLocation.add(mJsonObject.getString("service_location"));
                                        restaurantServices.add(mJsonObject.getString("service_description"));
                                        restaurantImages.add(mJsonObject.getString("featured_service_image"));
                                        break;
                                    case "Saloon":
                                        saloonNames.add(mJsonObject.getString("service_name"));
                                        saloonTelephone.add(mJsonObject.getString("service_phone"));
                                        saloonLocation.add(mJsonObject.getString("service_location"));
                                        saloonServices.add(mJsonObject.getString("service_description"));
                                        saloonImages.add(mJsonObject.getString("featured_service_image"));
                                        break;
                                    case "Mechanics":
                                        mechanicsNames.add(mJsonObject.getString("service_name"));
                                        mechanicsTelephone.add(mJsonObject.getString("service_phone"));
                                        mechanicsLocation.add(mJsonObject.getString("service_location"));
                                        mechanicsServices.add(mJsonObject.getString("service_description"));
                                        mechanicsImages.add(mJsonObject.getString("featured_service_image"));
                                        break;
                                    case "Bar":
                                        barsNames.add(mJsonObject.getString("service_name"));
                                        barsTelephone.add(mJsonObject.getString("service_phone"));
                                        barLocation.add(mJsonObject.getString("service_location"));
                                        barServices.add(mJsonObject.getString("service_description"));
                                        barImages.add(mJsonObject.getString("featured_service_image"));
                                        break;
                                    default:
                                        showErrorToast("Service list currently unavailable");
                                        break;
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            showErrorToast("Somthing went wrong");
                        }


                    aq.id(R.id.prog).gone();
                    aq.id(R.id.subCategoriesListView).visible();
                    determineArrayList(subCategoriesAdapter);
                    Log.d("Restaurant names", String.valueOf(restaurantNames));
                    Log.d("Restaurant names", String.valueOf(hotelImages));
                    Log.d("Hotel names", String.valueOf(hotelNames));
                    Log.d("Restaurant names", String.valueOf(restaurantImages));


                }else{
                    switch (status.getCode())
                    {

                        case AjaxStatus.TRANSFORM_ERROR:
                            Toast.makeText(aq.getContext(), "TRANSFORM_ERROR: " + status.getCode(), Toast.LENGTH_LONG).show();
                            break;
                        case AjaxStatus.NETWORK_ERROR:
                            Toast.makeText(aq.getContext(), "NETWORK_ERROR " + status.getCode(), Toast.LENGTH_LONG).show();
                            break;
                        case AjaxStatus.AUTH_ERROR:
                            Toast.makeText(aq.getContext(), "AUTH_ERROR" + status.getCode(), Toast.LENGTH_LONG).show();
                            break;
                        case AjaxStatus.NETWORK:
                            Toast.makeText(aq.getContext(), "NETWORK" + status.getCode(), Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Toast.makeText(aq.getContext(), "OTHER ERROR" + status.getCode(), Toast.LENGTH_LONG).show();
                            break;
                    }

                }
            }
        }.method(AQuery.METHOD_GET));

    }

    public void determineArrayList(SubCategoriesAdapter mAdapter){


        Intent intent = getIntent();
        String selectedCategory = intent.getStringExtra("Category");

        Log.d("Selected Cat", selectedCategory);

        if(selectedCategory.equals("Saloons")){
            Log.d("Selected Cat", "I get called");
            mAdapter = new SubCategoriesAdapter(SubCategories.this, saloonNames, saloonTelephone, saloonLocation, saloonServices, saloonImages);
        }else if(selectedCategory.equals("Hotels")){
            mAdapter = new SubCategoriesAdapter(SubCategories.this, hotelNames, hotelTelephone, hotelLocation, hotelServices, hotelImages);


        }else if(selectedCategory.equals("Restaurants")){
            mAdapter = new SubCategoriesAdapter(SubCategories.this, restaurantNames, resaurantTelephone, restaurantLocation, restaurantServices, restaurantImages);

        }else if(selectedCategory.equals("Mechanics")){
            mAdapter = new SubCategoriesAdapter(SubCategories.this, mechanicsNames, mechanicsTelephone, mechanicsLocation, mechanicsServices, mechanicsImages);

        }else if(selectedCategory.equals("Bars and Clubs")){
            mAdapter = new SubCategoriesAdapter(SubCategories.this, barsNames, barsTelephone, barLocation, barServices, barImages);
        }
        else{
            showErrorToast("List unavailable currently");
        }
        subCatListView.setAdapter(mAdapter);

    }


    private void showErrorToast(String error){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
