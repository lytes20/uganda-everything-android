package com.dashinvestments.serviceapp;

/**
 * Created by gideon_bamuleseyo on 11/4/17.
 */

public class Constants {

    public static String LOG_TAG = "MyUg";

    public static String DEFAULT_STRING = "N/A";

    public static String WEBSITE_URL = "http://myugandaapp.com/";

    public static String BASE_URL_PLAIN = "http://myugandaapp.com/dashboard/api/services/available_services";
}
