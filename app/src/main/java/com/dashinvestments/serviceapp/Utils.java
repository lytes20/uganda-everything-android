package com.dashinvestments.serviceapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gideon_bamuleseyo on 10/29/17.
 */

public class Utils {

    private static Pattern pattern;
    private static Matcher matcher;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean isValidEmail(String email){
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String formatDate(int year, int month, int day, String format){
        Calendar c = new GregorianCalendar(year, month, day);
        Date date = new Date(c.getTimeInMillis());
        return new SimpleDateFormat(format).format(date);
    }
}
