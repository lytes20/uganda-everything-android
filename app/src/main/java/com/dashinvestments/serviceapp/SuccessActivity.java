package com.dashinvestments.serviceapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.androidquery.AQuery;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gideon_bamuleseyo on 10/21/17.
 */

public class SuccessActivity extends AppCompatActivity {

    AQuery aq;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success_activty);

        aq = new AQuery(this);
        aq.id(R.id.success_bottom_container).gone();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
