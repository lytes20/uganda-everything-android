package com.dashinvestments.serviceapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by gideon_bamuleseyo on 10/8/17.
 */

public class CategoriesAdapter extends BaseAdapter {

    private final Context mContext;
    private LayoutInflater inflater;
    public String [] categories;
    public int [] catIcons;


    public CategoriesAdapter(Context mContext, String[] categories) {
        this.mContext = mContext;
        this.categories = categories;

    }

    public CategoriesAdapter(Context mContext, String[] categories, int[] catIcons) {
        this.mContext = mContext;
        this.categories = categories;
        this.catIcons = catIcons;
    }

    @Override
    public int getCount() {
        return categories.length;
    }

    @Override
    public Object getItem(int i) {
        return categories[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View gridView = view;
        if(view == null){

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.category_item, null);

        }
        TextView categoryName = (TextView) gridView.findViewById(R.id.categoryTextView);
        categoryName.setText(categories[i]);

        ImageView cateImage = gridView.findViewById(R.id.categoryImageView);
        cateImage.setImageResource(catIcons[i]);
        return gridView;
    }


}
