package com.dashinvestments.serviceapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by gideon_bamuleseyo on 10/26/17.
 */

public class BookingsAdapter extends BaseAdapter {

    private final Context mContext;
    private LayoutInflater inflater;
    public String [] services;

    public BookingsAdapter(Context mContext, String [] services) {
        this.mContext = mContext;
        this.services = services;
    }


    @Override
    public int getCount() {
        return services.length;
    }

    @Override
    public Object getItem(int i) {
        return services[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View listView = view;
        if(view == null){

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listView = inflater.inflate(R.layout.bookings_item, null);

        }
        TextView categoryName = (TextView) listView.findViewById(R.id.service_tv);
        categoryName.setText(services[i]);
        return listView;
    }
}
