package com.dashinvestments.serviceapp;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Created by gideon_bamuleseyo on 10/21/17.
 */

public class DefaultSpinnerAdapter extends ArrayAdapter<String>{
    public DefaultSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, String[] objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        view.setPadding(5, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        return view;
    }
}
