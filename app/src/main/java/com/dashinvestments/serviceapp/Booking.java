package com.dashinvestments.serviceapp;

/**
 * Created by gideon_bamuleseyo on 10/31/17.
 */

public class Booking {

    public String fullName;
    // public String userUserName;
    public String categoryName;
    public String selectedService;
    public String date;
    public String phone;
    public String timeSubmitted;
    public String selectedPlaceName;

    public Booking(){

    }

    public Booking(String mFullName,  String mCategoryName, String mSlectedService, String mDate, String phone, String timeSubmitted, String selectedPlaceName) {
        this.fullName = mFullName;
        //this.userUserName = mUserUserName;
        this.categoryName = mCategoryName;
        this.selectedService = mSlectedService;
        this.date = mDate;
        this.phone = phone;
        this.timeSubmitted = timeSubmitted;
        this.selectedPlaceName = selectedPlaceName;
    }
}
