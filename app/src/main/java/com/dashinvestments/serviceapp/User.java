package com.dashinvestments.serviceapp;

/**
 * Created by gideon_bamuleseyo on 10/27/17.
 */

public class User {
    private String firstName;
    private String lastName;
    private String username;
    private String phoneNumber;
    private String email;
    private String dob;
    private String sex;
    private String password;


    public User() {
    }

    public User(String fName, String lName){
        this.firstName = fName;
        this.lastName = lName;
    }

    public User(String firstName, String lastName, String username, String phoneNumber, String email, String dob, String sex, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.dob = dob;
        this.sex = sex;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
