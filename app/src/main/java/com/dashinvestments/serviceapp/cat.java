package com.dashinvestments.serviceapp;

import com.orm.SugarRecord;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by gideon_bamuleseyo on 11/5/17.
 */

public class cat extends SugarRecord implements Serializable {
    private static String TAG = cat.class.getSimpleName();

    public String sName;
    public String phone;
    public String location;
    public String email;

    public cat() {
    }

    public cat(String sName, String phone, String location, String email) {
        this.sName = sName;
        this.phone = phone;
        this.location = location;
        this.email = email;
    }

    public cat(JSONObject json){
        try{
            this.setId(json.getLong("id"));
            this.sName = json.getString("name");
            this.phone = json.getString("price");
            this.location = json.getString("sale_price");
        }catch (Exception ex){
            //Utils.log(TAG, "Error initializing price -> " + ex.getMessage());
        }
    }
}
